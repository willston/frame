//This will package your application for all operating systems at once.  Sick right?

var im = require('imagemagick');


  console.log("Converting image for Windows");
  im.convert([__dirname + "/app/icons/frameWindows.png", __dirname + '/app/icons/frame.ico'], 
  function(err, stdout){
    if (err) throw err;
    
  });


var packager = require('electron-packager');

packager({
    "dir": __dirname,
    "all": true,
    "app-copyright": "2017 Blue Bloc Designs.",
    "icon": __dirname + "/app/icons/frame",
    "out": __dirname + "/PUBLISH"

}, function done_callback (err, appPaths) { /* … */ })