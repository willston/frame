//Blue Bloc Designs Frame -- Its pretty dope.


var jsonfile = require('load-json-file');
var auto = require('autoconn-client');
const storage = require('electron-json-storage');
var fs = require('fs');
var frame = {
    module: {},
    info: {
        version: "1.0.0",
        module: {
            open: "none",
            last: "none"
        }
    },
    make: function(color) {
        if(color == "light") {
            $("html").animate({
                opacity: 0
            }, 1000, function() {
                var cssLink = $("<link id='whiteCSS' rel='stylesheet' type='text/css' href='./css/appWhite.css'>");
                $("head").append(cssLink); 
                $("html").animate({
                    opacity: 1
                }, 1000);
            });
        } else if(color == "dark") {
            $("html").animate({
                opacity: 0
            }, 1000, function() {
                $('#whiteCSS').remove();
                $("html").animate({
                    opacity: 1
                }, 1000);
            });
        } else if(color == "light-instant") {
            var cssLink = $("<link id='whiteCSS' rel='stylesheet' type='text/css' href='./css/appWhite.css'>");
            $("head").append(cssLink); 
        }
    },
    //This function executes as soon as the render process is ready and has loaded the HTML and all Javascript libraries.
    init: function () {
        if(options.theme == "light") {
           frame.make("light-instant");
        }
        $(document).foundation();
        frame.titlebar.init();
        frame.titlebar.loading();
        //Retrieve the Manifest file.  The Manifest file contains all of the modules
        jsonfile('./app/modules/manifest.json').then(json => {
         
            var id = options.loader.id;
            //Cycles through each module 
            $.each(json, function (index, value) {
                frame.module[json[index].name] = {
                    div: "modules" + index,
                    inf: json[index]
                }

                function loadCSS() {
                    //Load CSS File
                    preperation = ``;
                    try {
                        fs.readFile(__dirname + '/modules/' + json[index].dir + '/' + json[index].css, "utf8", function (err, data) {
                            if (err) {
                                frame.log("An error has occured. See above for details.");
                                frame.notify("Blue Bloc Framework Error 0x8", "Cannot find CSS File. See console for details.");
                                return console.log(err);
                            } else {
                                preperation = data;
                                preperation = `<style id='moduleCSS${index}'> ${preperation} </style>`;
                                console.log(preperation)
                                $('#' + id).html(preperation + $('#' + id).html());
                                loadHTML();
                            }
                        });
                    } catch (err) {

                        console.log(err);
                        frame.log("An error has occured. See above for details.");
                        frame.notify("Blue Bloc Framework Error 0x14", "An error occured with loading Javascript for modules.");

                    }
                }


                function loadHTML() {
                    //Load HTML File
                    preperation = ``;
                    try {
                        fs.readFile(__dirname + '/modules/' + json[index].dir + '/' + json[index].html, "utf8", function (err, data) {
                            if (err) {
                                frame.log("An error has occured. See above for details.");
                                frame.notify("Blue Bloc Framework Error 0x11", "Cannot find HTML File. See console for details.");
                                return console.log(err);
                            } else {
                                preperation = data;
                                preperation = `<div class='loaderContainer' id='modules${index}'> ${preperation} </div>`;
                                console.log(preperation)
                                $('#' + id).html(preperation + $('#' + id).html());
                                loadJS();
                            }
                        });
                    } catch (err) {

                        console.log(err);
                        frame.log("An error has occured. See above for details.");
                        frame.notify("Blue Bloc Framework Error 0x12", "A module did not load.  Check console for details.");

                    }
                }

                function loadJS() {
                    //Load Javascript File
                    preperation = ``;
                    try {
                        fs.readFile(__dirname + '/modules/' + json[index].dir + '/' + json[index].js, "utf8", function (err, data) {
                            if (err) {
                                frame.log("An error has occured. See above for details.");
                                frame.notify("Blue Bloc Framework Error 0x13", "Cannot find Javascript File. See console for details.");
                                return console.log(err);
                            } else {
                                preperation = data;
                                eval(preperation);
                               
                               
                            }
                        });
                    } catch (err) {

                        console.log(err);
                        frame.log("An error has occured. See above for details.");
                        frame.notify("Blue Bloc Framework Error 0x14", "An error occured with loading Javascript for modules.");

                    }
                }
                loadCSS();
            });
            
            setTimeout(function() {
                $('#title').animate({
                    opacity: 1
                }, 100);
                $('body').css({backgroundImage: "none"});
            }, 500);
            setTimeout(function() {
                $('#body').animate({
                    opacity: 1
                }, 100);
            }, 700);
            frame.log("Frame Initialized.");
            frame.titlebar.make(options.readymsg);
        });

    },
    //This function looks after the logging of messages
    log: function (msg) {
        console.log('MSG: ' + msg + '\n\tCreated at: ' + frame.time.hour() + ':' + frame.time.minute() + ':' + frame.time.second() + "." + frame.time.millisecond());
    },
    //This function gives you a bunch of preformatted time objects which makes logging and such really simple
    time: {


        year: function () {
            var d = new Date();
            var n = d.getFullYear();
            return n
        },
        month: function () {
            var d = new Date();
            var n = d.getMonth();
            return n
        },
        day: function () {
            var d = new Date();
            var n = d.getDate();
            return n
        },
        hour: function () {
            var d = new Date();
            var n = d.getHours();
            return n
        },
        minute: function () {
            var d = new Date();
            var n = d.getMinutes();
            return n
        },
        second: function () {
            var d = new Date();
            var n = d.getSeconds();
            return n
        },
        millisecond: function () {
            var d = new Date();
            var n = d.getMilliseconds();
            return n
        },
        timestamp: function () {
            var d = new Date();
            var n = d.getTime();
            return n
        }
    },
    //Sends a notification to the OS for handling.  The callback is only activated when you click on the notification!
    notify: function (title, msg, callback) {
        let myNotification = new Notification(title, {
            body: msg
        })
        myNotification.onclick = () => {
            frame.log('Notification clicked');
            if (!callback) callback = function () {}
            callback();
        }
    },
    //This function manipulates the title bar.
    titlebar: {
        //Creates the titlebar
        init: function () {
            frame.log("Setting up title bar.");
            if (options.titleBar.display) {
                $("#body").addClass("body-os");
                $("#titlebar").removeClass("hidden");
            } else {
                $("#titlebar").addClass("hidden");
                $("#body").removeClass("body-os");
            }
        },
        disp: function (option) {
            options.titleBar.display = option;
            frame.titlebar.init();
        },
        toggle: function () {
            if (options.titleBar.display == false) options.titleBar.display = true;
            else options.titleBar.display = false;
            frame.titlebar.init();
            return options.titleBar.display
        },
        loading: function () {
            $('#titlebar').html('<i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw"></i> <span style="width: 10px;"></span> Loading...  ');
        },
        make: function (text) {
            frame.log("Changing title to " + text);
            $('title').html(frame.stripHTML(text));
            $('#titlebar').html(text);
        }
    },
    stripHTML: function (html) {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    },
    load: function (moduleName, dataIn) {
        if(frame.info.module.open != "none") frame.FUNC[frame.info.module.open].closed();
        frame.info.module.last = frame.info.module.open;
        frame.info.module.open = moduleName;

        var id = "#" + options.loader.id;
        
        moduleDiv = "#" + frame.module[moduleName].div;


        if(options.loader.animations.use) {
            $(id).animate({
                opacity: 0
            }, options.loader.animations.time, function() {
                $('.loaderContainerVisible').removeClass('loaderContainerVisible');
                $(moduleDiv).addClass('loaderContainerVisible');
                if(options.loader.animations.use) {
                    $(id).animate({
                        opacity: 1
                    }, options.loader.animations.time)
                }
                frame.FUNC[moduleName].active(dataIn);
                });
             
        } else {
            $('.loaderContainerVisible').removeClass('loaderContainerVisible');
        }
        
       
    },
    FUNC: {
        //All the modules functions will be in here...
    }
}

