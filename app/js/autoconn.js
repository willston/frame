var auto = require("autoconn-client");
//
// This initializes the connection with the server.  If the aa key option is not provided the system will send in plain text over SSL.
//
var register = false; //On connection if this is true the device will be registered and then restart the render process.

//Checks to see if autoconn has been registered on this device yet or not.
storage.has('autoconn', function (error, hasKey) {
    if (error) throw error;
    if (hasKey) {
        storage.get("autoconn", function(error, data) {
            if(error) throw error;
            auto.conn.start({
                id: data.UID, //This is your devices unique identification number
                group: data.group, //This is your group number.  You can only see devices in your group using the QUERY, but you can talk to anyone.
                location: ["latitude", "longitude"], //This is your devices location on the planet.  Please be real with this.
                aa: "TESTING" //This is a key to encrypt and decrypt the data.  Only you know it!
            });
        });
    } else {
        auto.conn.start({
            id: "unregistered", //This is your devices unique identification number
            group: "0", //This is your group number.  You can only see devices in your group using the QUERY, but you can talk to anyone.
            location: ["latitude", "longitude"], //This is your devices location on the planet.  Please be real with this.
            aa: "somecode" //This is a key to encrypt and decrypt the data.  Only you know it!
        });
        register = true;
    }
});



//
// Anything in .begins happens when the server has made a connection.
//

auto.conn.begins = function (details) {
    //This will print the response from the server so
    //you can see all the data given to you.
    console.log(JSON.stringify(details, null, 4));


    //
    // .sms sends an SMS message.  You simply state the carrier the same
    // way as it is displayed in the title bar of your smart phone.
    //
/*
    auto.conn.sms({
        carrier: "VIRGIN", //Carrier as displayed on your phone.  Should be all caps.
        number: "6132433769", //The phone number.  No splace.  No symbols. Just numbers.
        message: "Testing AutoCONN EALS by Blue Bloc Designs" //Contents of the message. Less than 200 characters!
    });

    //
    // .send sends an SMS message.  You simply state the carrier the same
    // way as it is displayed in the title bar of your smart phone.
    //
*/
    /*auto.conn.send("REGISTRAR", {
        some: "sort of data can go here",
        it: "can be any sort of JSON or just a string even!",
        but: "its going to get encrypted if you use the aa key!"
    }, function (delivered, details) {
        //This function will run when a signature from the recieving device has been sent.
        //delivered can be "TRUE" or "FALSE", and details contains how long it took to
        //send the message as well as any responses from the device.
    });
*/

    auto.conn.requires.socket.client.on('REGISTERED', function (data) {
        console.log(JSON.stringify(data));

        storage.set('autoconn', data, function (error) {
            if (error) throw error
            frame.notify("AutoCONN// System", "AutoCONN Has registered this device with UID: " + data.UID);
            location.reload();
        });
    });
    if (register) {
        auto.conn.requires.socket.client.emit("REGISTER", {
            manu: "ca.blueblocdesigns",
            model: "Frame 1.1B",
            serial: "92510851",
            owner: "will@bluebloc.design",
            desc: "A test id",
            aa: 'NOT SUBMITTING'
        });
    }
}

//
// Anything in .rx happens when the server recieves a data packet.
// The data packet includes some data added headers.
//

auto.conn.rx = function (msg) {
    //This will print the response from the server so
    //you can see all the data given to you.
    frame.notify("Recieved AutoCONN Message", "Transmission took " + msg.took);
}

auto.conn.err = function (err) {
    //This will print the response from the server so
    //you can see all the data given to you.
    console.log(JSON.stringify(err, null, 4));
}

//The following is for Device Registration, it relies on the non-exposed AutoCONN// System functions.  Use carefully!
