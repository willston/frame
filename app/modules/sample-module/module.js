//This function will execute when this module is active in the window. The rest starts when the application loads
frame.FUNC["SampleModule"] = {
    active: function (data) {
        frame.notify("I've been activated!", "Thats right, I'm active!");
    },
    closed: function (data) {
        frame.notify("I've been deacivated!", "Thats right, I'm no longer active!");
    }
} 
