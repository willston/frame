//All your custom Javascript goes in here!
var options = {
    loader: {
        id: "everything",  //The DIV id of the content to be replaced.
        animations: {
            use: true,       //If we will use animations when loading?
            time: 500        //How long will we take to load it in, in ms
        }
    },
    titleBar: {
        display: true    //This toggles if the title bar is hidden or not.
    },
    theme: "light", //may also be dark... this loads another CSS file... thats it..
    readymsg: "Blue Bloc Designs | Frame"
}

frame.init(); //Starts up the frame functions